<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use App\Entity\Concert;
use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route("/admin", name:"admin")]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(ConcertCrudController::class)->generateUrl());
    }

    public function configureActions(): Actions
    {
        $actions = parent::configureActions();
        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
            return $action->setIcon('fa fa-edit')->setLabel(false);
        });

        $actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
            return $action->setIcon('fa fa-trash-alt')->setLabel(false);
        });

        return $actions;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de la Sirène');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('Retour au site', 'fa fa-sign-out-alt', 'app_homepage');
        yield MenuItem::section('Admin', 'fa fa-cogs');
        yield MenuItem::linkToCrud('Les concerts', 'fa fa-volume-up', Concert::class)->setController(ConcertCrudController::class);
        ;
        yield MenuItem::linkToCrud('Les concerts passés', 'fa fa-volume-down', Concert::class)->setController(ConcertPassesCrudController::class);
        yield MenuItem::linkToCrud('Les artistes', 'fa fa-user-cog', Artiste::class);
        yield MenuItem::linkToCrud('Newsletter', 'fa fa-paper-plane', Email::class);
    }

    public function configureCrud(): Crud
    {
        $crud =  parent::configureCrud();
        return $crud->showEntityActionsInlined();
    }
}
