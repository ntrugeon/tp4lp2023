<?php

namespace App\Controller\Admin;

use App\Entity\Concert;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;

class ConcertCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Concert::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['dateConcert' => 'ASC']);
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('artiste'),
            DateField::new('dateConcert'),
            MoneyField::new('tarif')->setCurrency('EUR')->setStoredAsCents(false), // si on ne veut stocker 12.90 par exemple et non 1290,
            TextEditorField::new('description'),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        $filters
            ->add('tarif');
        return $filters;
    }

// On a définit cela au niveau du dashboard
//    public function configureActions(Actions $actions): Actions
//    {
//        $actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
//            return $action->setIcon('fa fa-edit')->setLabel(false);
//        });
//
//        $actions->update(Crud::PAGE_INDEX,Action::DELETE,function (Action $action) {
//            return $action->setIcon('fa fa-trash-alt')->setLabel(false);
//        });
//
//        return $actions;
//    }


    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $requete = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters)
            ->andWhere('entity.dateConcert >= CURRENT_DATE()');
        return $requete;
    }
}
