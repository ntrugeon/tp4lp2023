<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use App\Field\VichImageField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ArtisteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Artiste::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
        yield TextField::new('nom')->hideOnIndex();
        yield TextField::new('prenom')->hideOnIndex();
        yield TextField::new('nomComplet')->hideOnForm();
        yield ImageField::new('photo')
            ->setBasePath('assets/imgGroupes')
            ->setUploadDir('public/assets/imgGroupes')
            ->setUploadedFileNamePattern('[randomhash].[extension]')
            ->setRequired(false);
        yield TextEditorField::new('biographie')->hideOnIndex();
    }
}
