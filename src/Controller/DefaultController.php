<?php

namespace App\Controller;

use App\Entity\Concert;
use App\Entity\Email;
use App\Form\NewsletterType;
use App\Repository\ArtisteRepository;
use App\Repository\ConcertRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function concerts(ConcertRepository $cr): Response
    {
        $concerts = $cr->concertsAVenir();
        $concertsPasses = $cr->concertsPasses();
        return $this->render('default/concerts.html.twig', [
            'concerts' => $concerts,
            'concertsPasses' => $concertsPasses,

        ]);
    }



    #[Route("/concert/{id}", name: "app_concert")]
    public function voirConcert(Concert $concert): Response
    {
        return $this->render('default/concert.html.twig', [
            'concert' => $concert

        ]);
    }

    #[Route("/artistes", name: "app_artistes")]
    public function allArtistes(ArtisteRepository $artisteRepository): Response
    {
        return $this->render('default/artistes.html.twig', [
            'artistes' => $artisteRepository->findAll()

        ]);
    }


    #[Route("/newsletter", name: "app_newsletter")]
    public function formNewsletter(Request $request, EntityManagerInterface $em): Response
    {
        $email = new Email();
        $form = $this->createForm(NewsletterType::class, $email);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($email);
            $em->flush();
            return $this->redirectToRoute('app_homepage');
        }
        return $this->render('default/_formNewsletter.html.twig', ['form' => $form]);
    }
}
