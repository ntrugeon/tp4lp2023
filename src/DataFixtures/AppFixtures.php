<?php

namespace App\DataFixtures;

use App\Entity\Artiste;
use App\Entity\Concert;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {}

    public function load(ObjectManager $manager)
    {

        $numPhoto=array(6,7,9,10,12,13,15,16);
        $nom=array('Wampas','Brain Damage','JP Manova','Sax Machine','The Stranglers','The Bad Plus','Maalouf','Fat Freddy\'s Drop');
        $prenom=array('Didier','','','','','','Ibrahim','');
        $artistes=array();
        $nb=count($numPhoto);
        for($i=0;$i<$nb;$i++) {
            $artiste = new Artiste(
                $nom[$i] . "",
                $prenom[$i] . "",
                $numPhoto[$i] . ".jpg",
                $nom[$i] . " Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
            );
            $artistes[]=$artiste;
            $manager->persist($artiste);
        }

        for($i=0;$i<$nb;$i++){
            $concert=new Concert(new \DateTime(mt_rand(-60,+60).' day'),"Super Concert",mt_rand(10,50));
            $concert->setArtiste($artistes[$i]);
            $manager->persist($concert);
        }

        $user = new Utilisateur();
        $user->setUsername('anthony');
        $user->setEmail('abourmau@univ-lr.fr');
        $user->setNom('Bourmaud');
        $user->setPrenom('Anthony');
        $user->setDateNaissance(new \DateTime('1980-05-01'));
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $manager->persist($user);

        $admin = new Utilisateur();
        $admin->setUsername('admin');
        $admin->setEmail('ntrugeon@univ-lr.fr');
        $admin->setNom('Trugeon');
        $admin->setPrenom('Nicolas');
        $admin->setDateNaissance(new \DateTime('1980-05-01'));
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPassword($this->passwordHasher->hashPassword($user, 'admin'));
        $manager->persist($admin);

        $manager->flush();
    }
}
